
import os
import sys
import site

site.addsitedir('/var/www/data/virtualenv/lib/python3.4/site-packages')

sys.path.append('/var/www/data/rave_project')
sys.path.append('/var/www/data/rave_project/rave_project')

#os.environ['DJANGO_SETTINGS_MODULE'] = 'rave_project.settings'

activate_env = os.path.expanduser('/var/www/data/virtualenv/bin/activate_this.py')

exec(open(activate_env).read(),dict(__file__=activate_env))

#import django.core.handlers.wsgi
#application = django.core.handlers.wsgi.WSGIHandler()
from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "tyr_project.settings")

application = get_wsgi_application()
