import os
import sys
import site

site.addsitedir('/var/www/data/venv/lib/python3.5/site-packages')

sys.path.append('/var/www/data/tyr')
sys.path.append('/var/www/data/tyr/tyr')

os.environ['DJANGO_SETTINGS_MODULE'] = 'tyr.settings'

activate_env = os.path.expanduser('/var/www/data/venv/bin/activate_this.py')

exec(open(activate_env).read(),dict(__file__=activate_env))

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "tyr.settings")

application = get_wsgi_application()
