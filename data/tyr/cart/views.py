# Create your views here.
# views.py
from cart.cart import Cart, Item
# from myproducts.models import Product
from django.http import JsonResponse
from django.shortcuts import render
from catalog.models import Exam


def add_to_cart(request):
    if request.method == "POST":
        product_id = request.POST.get('id')

        anno = request.POST.get('anno')
        cfu = request.POST.get('cfu')
        codice = request.POST.get('codice')
        nome = request.POST.get('name')
        sede = request.POST.get('sede')
        argomenti = request.POST.get('argomenti')

        product = Exam.objects.get(id_exam=product_id)
        # product = Product.objects.get(id=product_id)
        cart = Cart(request)

        cart.add(anno, codice, nome, sede, argomenti, product, cfu, 1)
        number = cart.count()
        return JsonResponse(
            {"succces": "Created",
             "count": number,
             },
            status=200
        )
    else:
        return JsonResponse(
            {'errors': 'Method Not Allowed'})


def remove_from_cart(request):
    if request.method == "POST":
        product_id = request.POST.get('id')
        product = Exam.objects.get(id_exam=product_id)
        # product = Product.objects.get(id=product_id)
        cart = Cart(request)
        cart.remove(product)
        number = cart.count()
        return JsonResponse(
            {"succces": "Delete",
             "count": number,
             },
            status=200
        )
    else:
        return JsonResponse(
            {"errors": {"Method Not Allowed"}},
            status=405
        )


def get_cart(request):
    if request.method == "GET":
        cart = Cart(request)
        c = cart.cart_serializable()
        return JsonResponse({'cart': c, 'nterm': cart.count()}, status=200)
    else:
        return JsonResponse(
            {"errors": {"Method Not Allowed"}},
            status=405
        )


def vista_cart(request):
    if request.method == 'GET':
        cart = Cart(request)
        d = cart.is_cart()
        return JsonResponse({'cart': d, 'term': cart.count()}, status=200)
    else:
        return JsonResponse(
            {"errors": {"Method Not Allowed"}},
            status=405
        )
