import datetime
from catalog.models import Exam, Exambo
from django.db.models import Q
from . import models
from cart.models import Item
CART_ID = 'CART-ID'


class ItemAlreadyExists(Exception):
    pass


class ItemDoesNotExist(Exception):
    pass


class Cart:
    def __init__(self, request):
        cart_id = request.session.get(CART_ID)
        if cart_id:
            try:
                cart = models.Cart.objects.get(id=cart_id, checked_out=False)
            except models.Cart.DoesNotExist:
                cart = self.new(request)
        else:
            cart = self.new(request)
        self.cart = cart

    def __iter__(self):
        for item in self.cart.item_set.all():
            yield item

    def new(self, request):
        cart = models.Cart(creation_date=datetime.datetime.now())
        cart.save()
        request.session[CART_ID] = cart.id
        return cart

    def add(self, anno, codice, nome, sede, argomenti, product, unit_price, quantity=1):
        try:
            item = models.Item.objects.get(
                cart=self.cart,
                product=product,
            )
        except models.Item.DoesNotExist:
            item = models.Item()
            item.cart = self.cart
            #valori da inserire nel carrello
            item.sede_corso = sede
            item.nome_corso = nome
            item.codice_corso = codice
            item.anno_corso = anno
            item.argomenti_corso = argomenti

            item.product = product
            item.unit_price = unit_price
            item.quantity = quantity
            item.save()
        else:  # ItemAlreadyExists
            item.unit_price = unit_price
            item.quantity = int(quantity)
            item.save()

    def remove(self, product):
        try:
            item = models.Item.objects.get(
                cart=self.cart,
                product=product,
            )
        except models.Item.DoesNotExist:
            raise ItemDoesNotExist
        else:
            item.delete()

    def update(self, product, quantity, unit_price=None):
        try:
            item = models.Item.objects.get(
                cart=self.cart,
                product=product,
            )
        except models.Item.DoesNotExist:
            raise ItemDoesNotExist
        else:  # ItemAlreadyExists
            if quantity == 0:
                item.delete()
            else:
                item.unit_price = unit_price
                item.quantity = int(quantity)
                item.save()

    def count(self):
        result = 0
        for item in self.cart.item_set.all():
            result += 1
        return result

    def summary(self):
        result = 0
        for item in self.cart.item_set.all():
            result += item.total_price
        return result

    def clear(self):
        for item in self.cart.item_set.all():
            item.delete()

    def is_empty(self):
        return self.count() == 0

    def cart_serializable(self):
        representation = []
        for item in self.cart.item_set.all():
            tem = Exam.objects.filter(Q(id_exam=item.object_id))
            exambo = tem[0].esame_bo
            itemToDict = [{
                'id_bo': exambo.id_bo,
                'nome': item.nome_corso,
                'cfu': item.unit_price,
                'id': item.object_id
            }]
            representation += itemToDict
        return representation

    def is_cart(self):
        representation = []
        for item in self.cart.item_set.all():
            tem = Exam.objects.filter(Q(id_exam=item.object_id))
            exambo = tem[0].esame_bo
            itemToDict = [{
                'id_exam': tem[0].id_exam,
                'sede_corso': item.sede_corso,
                'nome_corso': item.nome_corso,
                'codice_corso': item.codice_corso,
                'anno_corso': item.anno_corso,
                'argomenti_corso': item.argomenti_corso,
                'integrazioni_corso': str(tem[0].integrazioni_corso),
                'cfu_corso': item.unit_price,
                'esame_bo': {
                    'id_bo': exambo.id_bo,
                    'codice_bo': exambo.codice_bo,
                    'name_bo': exambo.name_bo,
                    'anno_bo': exambo.anno_bo,
                    'argomenti_bo': exambo.argomenti_bo,
                    'cfu_bo': exambo.cfu_bo,
                    'ssd_bo': exambo.ssd_bo,
                    'sede_bo': exambo.sede_bo,
                },
            }]
            representation += itemToDict
        return representation
