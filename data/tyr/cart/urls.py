from __future__ import unicode_literals
from django.conf.urls import url
from cart.views import get_cart, add_to_cart, remove_from_cart, vista_cart

urlpatterns = [
    url(r"^$", vista_cart, name="cart"),
    url(r"^add/$", add_to_cart, name="index_update_create"),
    url(r"^rm/$", remove_from_cart, name="delete"),
    url(r'^views/$', get_cart, name="read"),

]
