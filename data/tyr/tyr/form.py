from __future__ import (
    absolute_import, division, print_function, unicode_literals
)
from datetime import datetime
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, ButtonHolder, Submit, Reset, Button
from django import forms
from django.contrib.auth import get_user_model, authenticate
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.forms.utils import ErrorList
from django.core.validators import MaxValueValidator, MinValueValidator
from catalog.models import Exambo
import uuid

oggi = datetime.now()


def current_year():
    return oggi.year


def year_choices():
    return [(r, str(r) + '/' + str(r + 1)) for r in range(oggi.year, 1999, -1)]


class LoginForm(AuthenticationForm):
    helper = FormHelper()
    helper.form_tag = False

    def __init__(self, request=None, *args, **kwargs):
        super(LoginForm, self).__init__(request, *args, **kwargs)
        self.error_class = ErrorListBootstrap
        self.fields.keyOrder = ["username", "password"]

    def clean_username(self):
        user_model = get_user_model()
        username = self.cleaned_data["username"]
        try:
            user_model.objects.get(username__iexact=username)
        except user_model.DoesNotExist:
            raise forms.ValidationError(
                "This user doesn't exists.",
                code="username_not_registered"
            )
        return username

    def clean_password(self):
        username = self.cleaned_data.get("username", None)
        password = self.cleaned_data["password"]

        if username and authenticate(
                username=username,
                password=password
        ) is None:
            raise forms.ValidationError(
                "Failed login please check password",
                code="wrong_password"
            )
        return password


class ErrorListBootstrap(ErrorList):
    def __str__(self):
        return self.as_simple_list()

    def as_list(self):
        if not self:
            return ""
        return "".join(["<li>{}</li>".format(e) for e in self])

    def as_simple_list(self):
        if not self:
            return ""
        return "".join(["{}</br>".format(e) for e in self])


class SearchForm(forms.Form):
    codice = forms.IntegerField(required=False)
    esame = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'nome-esame'}))
    sede = forms.CharField(required=False, widget=forms.TextInput(attrs={'placeholder': 'Università'}))
    cfu = forms.IntegerField(required=False, validators=[MinValueValidator(1), MaxValueValidator(200)])
    anno = forms.TypedChoiceField(required=False, coerce=int, choices=year_choices, initial=current_year)
    argomenti = forms.CharField(required=False,
                                widget=forms.TextInput(attrs={'placeholder': 'Argomenti separati da ","'}))
    helper = FormHelper()
    helper.form_tag = False

    def __init__(self, request=None, *args, **kwargs):
        super(SearchForm, self).__init__(request, *args, **kwargs)
        self.helper.form_class = 'form-horizontal'
        self.helper.layout = Layout(
            Field('codice', ),
            Field('esame'),
            Field('sede'),
            Field('cfu'),
            Field('anno'),
            Field('argomenti'),
        )


class ExamboForm(forms.ModelForm):
    anno_bo = forms.TypedChoiceField(coerce=int, choices=year_choices, initial=current_year)

    helper = FormHelper()

    class Meta:
        model = Exambo
        fields = ["codice_bo", "name_bo", "anno_bo", "argomenti_bo", "cfu_bo", "ssd_bo"]

    def __init__(self, *args, **kwargs):
        super(ExamboForm, self).__init__(*args, **kwargs)
        # If you pass FormHelper constructor a form instance
        # It builds a default layout with all its fields
        self.helper.form_method = 'POST'
        self.helper.form_action = '/catalog/exambo/'

        # You can dynamically adjust your layout
        self.helper.form_class = 'form-horizontal'
        # NEW:
        self.helper.label_class = 'col-sm-2'
        self.helper.field_class = 'col-sm-10'

        self.helper.layout = Layout(
            Field('codice_bo', css_class='input-xlarge'),
            Field('ssd_bo', css_class='input-xlarge'),
            Field('name_bo', css_class='input-xlarge'),
            Field('anno_bo', css_class='input-xlarge'),
            Field('cfu_bo', css_class='input-xlarge'),
            Field('argomenti_bo', css_class='input-xlarge'),

            # NEW:
            ButtonHolder(
                Reset('reset', 'reset', css_class='btn btn-outline-danger'),
                Submit('submit', 'submit', css_class='btn btn-outline-success')
            )
        )
