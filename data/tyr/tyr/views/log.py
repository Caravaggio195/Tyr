# -*- coding: utf-8 -*-!
""" User Management """
from __future__ import unicode_literals
from tyr.form import LoginForm, SearchForm
from django.shortcuts import render
from django.contrib.auth import login, logout
from django.http import JsonResponse
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required


def loginuser(request, ):
    if request.user.is_authenticated:
        return render(
            request=request,
            template_name="login.html",
            context={
                "title": "Search",
                "type": "GET",
                "submitlink": "/catalog/search/",
                "submit": "Search",
                "cart": True,
                "login_form": SearchForm()
            }

        )
    else:
        return render(
            request=request,
            template_name="login.html",
            context={
                "title": "Home",
                "type": "POST",
                "submitlink": "/login/",
                "submit": "Login",
                "login_form": LoginForm(prefix="login")
            }
        )


def loginu(request):
    """log user """
    # just need user validation so pass None as request
    form = LoginForm(None, request.POST)

    response = JsonResponse(
        {"errors": {"": ["This form requires a POST request"]}},
        status=405
    )
    if form:
        if form.is_valid():
            login(request, form.get_user())
            response = JsonResponse({"result": "Logged in"}, status=200)

        else:  # invalid
            errors = {}

            for e in form.errors:
                errors[e] = str(form[e].errors)

            for err in form.non_field_errors():
                errors["login"] = str(err)

            response = JsonResponse({"errors": errors}, status=400)
    return response


def logoutu(request):
    """out """
    logout(request)
    return redirect("/home/")


def cartw(request):
    if request.user.is_authenticated:
        return render(
            request=request,
            template_name="cartw.html",
            context={
                "title": "Cart",
            }
        )