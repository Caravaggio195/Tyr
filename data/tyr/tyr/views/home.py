# -*- coding: utf-8 -*-!
from __future__ import unicode_literals
from django.shortcuts import render, redirect
from tyr.form import ExamboForm
def home(request):
    """Home page"""
    return render(request, "base.html", {'title': 'home'})
