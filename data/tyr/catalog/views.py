from __future__ import unicode_literals
import json
from django.conf import settings
from django.shortcuts import redirect
from django.http.response import JsonResponse, HttpResponse
from django.views.generic.base import View
import catalog
from catalog.models import Exambo, Integrazione, Exam
from tyr.form import ExamboForm
from django.http import HttpResponseRedirect
from django.shortcuts import render
from catalog.catalog import searchExam
from django.db.models import Q


class EsamiRootView(View):
    def get(self, second):
        return JsonResponse({
            "name": getattr(
                settings, "EsamiSherch_NAME",
                "django-Esami-store"
            ),
            "version": catalog.__version__
        })


def SearchView(request, *args, **kwargs):
    if request.method == "GET":
        sede = request.GET['sede']
        nome = request.GET['name']
        cod = request.GET['codice']
        anno = int(request.GET['anno'])
        cfu = int(request.GET['cfu'])
        argomenti = request.GET['argomenti']
        temporale = searchExam(sede, nome, cod, anno, argomenti, cfu)
        return JsonResponse({
            "item": len(temporale),
            "data": temporale
        })

def SID(request):
    if request.method == "GET":
        idcorso = request.GET['id']
        temporale = models.Exam.objects.filter(Q(id_corso=idcorso))
        return JsonResponse({
            "nome": temporale[0].nome_corso,
            "cfu": temporale[0].cfu_corso,
            "sede": temporale[0].sede_corso,
            "riconosciuto": str(temporale[0].esame_bo),
            "note": str(temporale[0].integrazioni_corso),
            "anno": temporale[0].anno_corso,
            "cack": 1,
            "id": temporale[0].id_exam
        })


def InsertView(request):
    if request.method == "POST":
        form = ExamboForm(request.POST)
        if form.is_valid():
            exam2 = Exambo.objects.filter(sede_bo='Bologna')
            print(exam2)
            exam1 = form.save()
            form.save()
            return redirect(InsertView)
        else:
            return redirect(InsertView)
    if request.method == "GET":
        return render(
            request=request,
            template_name="inserisci.html",
            context={
                "title": "Home",
                "form": ExamboForm()
            }
        )
