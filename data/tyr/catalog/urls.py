from __future__ import unicode_literals

from django.conf.urls import url

from catalog import views
urlpatterns = [
    url(r"^$", views.EsamiRootView.as_view(), name="root"),
    url(r'^search/$', views.SearchView, name="search"),
    url(r'^exambo/$', views.InsertView, name="esamibo"),
    url(r'^id/$', views.SID, name="ricerca per id"),
]
