import datetime
from django.db.models.functions import Greatest
from django.db.models import Max
from . import models
from django.db.models import Q

'''Cerca gli esami di uno stesso ateneo'''

temporale2 = []


def searchExam(sede, nome, codice, anno, argomenti, cfu):
    '''Cerca eventali ogetti identici o coumnque piu simili possibili'''
    argomento = argomenti.split(',')
    for num in range(0, len(argomento) - 1):
        if argomento[num] == '':
            argomento.pop(num)
    item = models.Exam.objects.filter(Q(sede_corso=sede) &
                                      Q(nome_corso=nome) &
                                      Q(anno_corso=anno) &
                                      Q(codice_corso=codice) &
                                      Q(cfu_corso=cfu)
                                      ).distinct()
    if item.count() == 0:
        '''stesso nome anni diversi '''
        item = models.Exam.objects.filter(Q(sede_corso=sede) &
                                          Q(nome_corso=nome) &
                                          (Q(anno_corso=anno) |
                                           Q(codice_corso=codice))
                                          ).distinct()
    if item.count() == 0:
        '''stesso nome anni diversi '''
        item = models.Exam.objects.filter(Q(sede_corso=sede) & Q(nome_corso=nome)).distinct()
    if item.count() == 0:
        temporale2 = []
        '''se non trova nente ricerchiamo per similitudini'''
        for i in argomento:
            exam_name = models.Exam.objects.filter(Q(sede_corso=sede))

            exam_arg = models.Exam.objects.filter(Q(nome_corso__icontains=nome) |
                                                  Q(argomenti_corso__icontains=i)
                                                  )
            temporale = (exam_name & exam_arg).distinct()
            for numero in range(len(temporale)):
                exambo = temporale[numero].esame_bo

                temporale2 = temporale2 + [{
                    "nome": temporale[numero].nome_corso,
                    "cfu": temporale[numero].cfu_corso,
                    "sede": temporale[numero].sede_corso,
                    "riconosciuto": str(temporale[numero].esame_bo),
                    "note": str(temporale[numero].integrazioni_corso),
                    "anno": temporale[numero].anno_corso,
                    "cack": 1,
                    "id": temporale[numero].id_exam,
                    'id_bo': exambo.id_bo,
                    'distance': abs(temporale[numero].anno_corso - exambo.anno_bo),

                }]
    else:
        temporale2 = []
        temporale = item
        for numero in range(len(temporale)):
            exambo = temporale[numero].esame_bo
            temporale2 = temporale2 + [{
                "nome": temporale[numero].nome_corso,
                "cfu": temporale[numero].cfu_corso,
                "sede": temporale[numero].sede_corso,
                "riconosciuto": str(temporale[numero].esame_bo),
                "note": str(temporale[numero].integrazioni_corso),
                "anno": temporale[numero].anno_corso,
                "cack": 1,
                "id": temporale[numero].id_exam,
                'id_bo': exambo.id_bo,
                'distance': abs(temporale[numero].anno_corso - exambo.anno_bo),
            }]
    seq = temporale2
    # aggiungi occorrenze
    occorreze = []
    for z in range(len(seq) - 1, 0, -1):
        i = 1
        for m in range(z, 0, -1):
            if (seq[z]['id'] == seq[m]['id']):
                i += 1
        occorreze += [{'id': seq[z]['id'], 'cack': i}]
    # remove duplicates
    res_list = []
    for i in range(len(seq)):

        if seq[i] not in seq[i + 1:]:
            res_list.append(seq[i])
    seq = res_list
    # aggiungi occorrenze a lista non duplicata
    for k in range(len(occorreze)):
        for s in range(len(seq)):
            if (seq[s]['id'] == occorreze[k]['id']) & (seq[s]['cack'] < occorreze[k]['cack']):
                print('ecco')
                seq[s]['cack'] = occorreze[k]['cack']
                print(seq[s])
                print('\n')
    lengthOfArray = len(seq) - 1
    for i in range(lengthOfArray):
        for j in range(lengthOfArray - i):
            if seq[j]['cack'] < seq[j + 1]['cack']:
                seq[j], seq[j + 1] = seq[j + 1], seq[j]

    return seq
