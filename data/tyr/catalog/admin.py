from django.contrib import admin
from catalog.models import Integrazione, Exam, Exambo
from cart.models import Cart, Item
admin.site.register(Integrazione)
admin.site.register(Exam)
admin.site.register(Exambo)
admin.site.register(Cart)
admin.site.register(Item)
# Register your models here.
