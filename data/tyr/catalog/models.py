from django.db import models
from django.db.models import PositiveIntegerField
from django.urls import reverse
from django.core.validators import MaxValueValidator, MinValueValidator
import uuid
import datetime


def current_year():
    return datetime.date.today().year


def max_value_current_year(value):
    return MaxValueValidator(current_year())(value)


# Create your models here.
class Integrazione(models.Model):
    """Model sede"""
    name_integrazione = models.TextField(max_length=2000, help_text='integrazione esame', null=True, blank=True)

    def __str__(self):
        return self.name_integrazione


class Exambo(models.Model):
    id_bo = models.CharField(max_length=60, primary_key=True, default=uuid.uuid4, editable=False)
    codice_bo = models.CharField(max_length=20)
    name_bo = models.CharField(max_length=200)
    anno_bo = models.PositiveIntegerField(validators=[MinValueValidator(1900), MaxValueValidator(2200)],
                                          help_text='anno Accademico(es. 2019/2020--->2019)')
    argomenti_bo = models.CharField(max_length=2000, null=True, blank=True,
                                    help_text='argomenti corso (es. Aloritmi paralleli, matematica,.. ecc.)', )
    cfu_bo = models.PositiveIntegerField(help_text='Numero CFU', validators=[MaxValueValidator(200)])
    ssd_bo = models.CharField(max_length=50, null=True, blank=True, help_text='ssd', )
    sede_bo = models.CharField(default="Bologna", max_length=100, editable=False)

    class Meta:
        ordering = ['name_bo']

    def __str__(self):
        """String for representing the Model object."""
        return self.name_bo

    def get_absolute_url(self):
        """Returns the url to access a exam"""
        return reverse('exam-detail', args=[str(self.name_bo)])


class Exam(models.Model):
    esame_bo = models.ForeignKey(Exambo, related_name='esame_bo')
    id_exam = models.CharField(max_length=60, primary_key=True, default=uuid.uuid4, editable=False)
    sede_corso = models.CharField(max_length=100, null=True, blank=True, help_text='sede esame', )
    nome_corso = models.CharField(max_length=200)
    codice_corso = models.CharField(max_length=20)
    anno_corso = models.PositiveIntegerField(validators=[MinValueValidator(1900), MaxValueValidator(2200)],
                                             help_text='anno Accademico(es. 2019/2020--->2019)')
    argomenti_corso = models.CharField(max_length=2000, null=True, blank=True,
                                       help_text='argomenti corso (es. Aloritmi paralleli, matematica,.. ecc.)', )
    integrazioni_corso = models.ForeignKey(Integrazione, null=True, blank=True, related_name='integrazione')
    cfu_corso = models.PositiveIntegerField(help_text='Numero CFU', validators=[MaxValueValidator(200)])

    class Meta:
        ordering = ['nome_corso']

    def __str__(self):
        """String for representing the Model object."""
        return self.nome_corso

    def get_absolute_url(self):
        """Returns the url to access a exam"""
        return reverse('exam-detail', args=[str(self.nome_corso)])
