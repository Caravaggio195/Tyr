var data = new Date();
var urlS = window.location.origin;
var $search;//<-------------    var di ricerca
var esami;//<---------------    var di ricerca
var html1;//<---------------    var manipolazione pagine
var del_cart;//<------------    var da inserire nel carrello
var blacklist = {};//<------    var da non visualizzare nella ricerca
var c;//<-------------------    var da aggiungere al carello

//<<<<<<<<<<<<<<<<<<<<<CSRF>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function sameOrigin(url) {
    var host = document.location.host;
    var protocol = document.location.protocol;
    var sr_origin = "//" + host;
    var origin = protocol + sr_origin;
    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
        (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||

        !(/^(\/\/|http:|https:).*/.test(url));
}

$.ajaxSetup({
    beforeSend: function (xhr, settings) {
        if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
            xhr.setRequestHeader("X-CSRFToken", Cookies.get("csrftoken"));
        }
    }
});

//<<<<<<<<<<<<<<<<<<<<<termini ricerca>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
function initialize() { //<---inserire cfu e anno attuale
    $('input[id="id_codice"]').val();
    $("input[id='id_esame']").val('');
    $("input[id='id_sede']").val('');
    $("input[id='id_anno']").val(data.getFullYear());
    $("select[id='id_cfu']").val(6);
    $("input[id='id_argomenti']").val('');
}

function terms() {//<--- dato da invire con ricerca
    $search = {
        codice: Number($('input[id="id_codice"]').val()) ,
        name: $('input[id="id_esame"]').val().toLowerCase(),
        sede: $('input[id="id_sede"]').val().toLowerCase(),
        cfu: Number($('input[id="id_cfu"]').val()),
        anno: Number($('select[id="id_anno"]').val()),
        argomenti: $("input[id='id_argomenti']").val().toLowerCase()
    };
    console.log($search); //<--TEST
}

function insertblacklist(ter, i) {
    if (i === true) {//<---inserisci o elimina

        blacklist[ter] = true;
        console.log(blacklist);
    }
    if (i === false) {
        delete blacklist[ter];
    }
}

//<<<<<<<<<<<<<<<<<<<<<ricerca>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
function risulta(esami) { //<-----stampa i risultati
    if (esami.item < 1) return -1;
    else {
        var tabella = false;
        html1 = '<div class="table-responsive"><table class="table table-hover"> <thead><tr><th scope="col">Esame</th><th scope="col">CFU</th><th scope="col">Sede</th><th>Riconosciuto con</th><th scope="col">Note</th><th scope="col">Seleziona</th></tr></thead><tbody>';
        for (var i = 0; i < esami.item; i++) {
            if ((!(esami.data[i].id in blacklist)) & (!(esami.data[i].id_bo in blacklist))) {
                tabella = true;
                html1 += '<tr id="' + esami.data[i].id + '"';
                if (esami.data[i].distance >= 3) {
                    html1 += 'class="table-warning"';
                }
                html1 += '>';
                html1 += '<th>' + esami.data[i].nome + '</th> <th>' + esami.data[i].cfu + '</th><th>' + esami.data[i].sede + '</th><th>' + esami.data[i].riconosciuto + '</th><th>' + esami.data[i].note + '</th>' +
                    '<th>' +
                    '<div class="form-check">'+
                    '<input class="form-check-input" type="checkbox" value="' + esami.data[i].id + '" id="' + esami.data[i].id_bo + '" >\n' +
                    '</div>'+
                    '</th>';
                html1 += '</tr>';
            }
        }

        html1 += '</tbody></table></div>';
        if (tabella === true) {
            $("#ris").html(html1);
            c = $("input.form-check-input");
        }
    }
    add_cart();
}

function cerca() {  //<---ricerca su database EXAM
    $.ajax({
        url: urlS + '/catalog/search/',
        type: 'GET',
        data: $search,
        success: function (esa) {
            esami = esa;
            risulta(esami);
        },
        error: function (error) {
            console.log(error);
            alert("Wrong");
        }
    });
}

function ricerca() {
    terms();
    cerca();
}

//<<<<<<<<<<<<<<<<<<<<<carello>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
function articolo(a) {
    return Object.assign({}, {'id': a}, $search);
}


function get_cart() {  //<---ajax ricerca su database
    $.ajax({
        url: urlS + '/cart/views/',
        type: 'GET',
        success: function (data) {
            if (data.nterm < 0) return -1;
            else {
                var total_cfu = 0;
                var html = '';
                if (data.nterm > 0) {
                    for (var i = 0; i < data.nterm; i++) {
                        insertblacklist(data.cart[i].id, true);
                        insertblacklist(data.cart[i].id_bo, true);
                        //
                        total_cfu += data.cart[i].cfu;
                        html += '<div class="col-lg-12 col-sm-12 col-12 cart-detail-product">\n' +
                            '<span>' + data.cart[i].nome + '</span>\n' +
                            '<span class="price text-info"> ' + data.cart[i].cfu + '</span>\n' +
                            '<button class="btn btn-danger btn-sm" data-id="' + data.cart[i].id + '"id="' + data.cart[i].id_bo + '" ><i class="fas fa-window-close"></i></button>\n' +
                            '</div>'
                    }
                }

                $('#totalexam1').html(data.nterm);
                $('#totalexam').html(data.nterm);
                $('#totalcfu').html(total_cfu);
                $("#cart-detail").html(html);
                del_cart = $('#cart-detail > div > button');
                console.log(del_cart);
                rem_cart();
            }
        },
        error: function (error) {
            console.log(error);
            alert("Wrong");
        }
    });
}

function aggiungi_esame(aggiungie) {  //<---ajax esame a carello (funziona)
console.log(aggiungie);
    $.ajax({
        url: urlS + '/cart/add/',
        type: 'POST',
        data: aggiungie,
        success: function (esa) {
            console.log(esa);
            get_cart();
        },
        error: function (error) {
            console.log(error);
            alert("Wrong");
        }
    });
}

function rimuovi_esame(del) {
    $.ajax({
        url: urlS + '/cart/rm/',
        type: 'POST',
        data: del,
        success: function (esa) {
            console.log(esa);
            get_cart();
        },
        error: function (error) {
            console.log(error);
            alert("Wrong");
        }
    });
}

function rem_cart() {

    $(del_cart).click(function (evt) {
        evt.preventDefault();
        insertblacklist($(this).data("id"), false);
        insertblacklist(this.id, false);
        rimuovi_esame({'id':$(this).data("id")});
        $(this).parent().remove();

    });
}

function add_cart() {
    $(c).change(function () {
        var duplicato = true;
        if (this.checked) {
            if ((this.id in blacklist) || (this.value in blacklist)) {
                duplicato = confirm("Sta inserendo un esame già riconosciuto");
            }
            if (duplicato === true) {
                insertblacklist(this.id, true);
                insertblacklist(this.value, true);
                console.log(this.value);
                console.log(articolo(this.value));
                $(this).parent().parent().parent().remove();
                aggiungi_esame(articolo(this.value));


            } else {
                $(this).prop("checked", false);
            }
        }


    });

}

//<<<<<<<<<<<<<<<<<<<<<caricamento documento>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
$(document).ready(function () {
    initialize();
    terms();
    get_cart();

    $("#login_form").on("submit", function (evt) {
        evt.preventDefault();
        ricerca();

    });


    $("#login_form").on("reset", function (evt) {
        evt.preventDefault();
        initialize();

    });

    $('#btncart').click(function (evt) {
            var collapse = $('body > nav > div > button').attr('aria-expanded');
            if (collapse === 'true') {
                $('#dropdown-cart').css("left", "0px");
            }
        else {
            $('#dropdown-cart').css("left", "-160px");
        }
    });

});