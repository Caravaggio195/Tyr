var urlS = 'http://domenico.caravaggio3.tw.cs.unibo.it';
var hatml1 = '';

//<<<<<<<<<<<<<<<<<<<<<CSRF>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function sameOrigin(url) {
    var host = document.location.host;
    var protocol = document.location.protocol;
    var sr_origin = "//" + host;
    var origin = protocol + sr_origin;
    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
        (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||

        !(/^(\/\/|http:|https:).*/.test(url));
}

$.ajaxSetup({
    beforeSend: function (xhr, settings) {
        if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
            xhr.setRequestHeader("X-CSRFToken", Cookies.get("csrftoken"));
        }
    }
});

//-----------------------------------------------------------------

function get_cart() {  //<---ajax
    $.ajax({
        url: urlS + '/cart/',
        type: 'GET',
        success: function (table) {
            console.log(table);
            for (var i = 0; i < table.term; i++) {
                var esame = table.cart[i];
                var anno=table.cart[i].anno_corso;
                console.log(anno);
                hatml1+='<tr>';
                hatml1 += '<th scope="row">' + esame.nome_corso + '</th>';
                hatml1 += '<th>' + esame.codice_corso + '</th>';
                hatml1 += '<th>' + anno + '/' + (anno+1) + '</th>';
                hatml1 += '<th>' + esame.cfu_corso + '</th>';
                hatml1 += '<th>' + esame.sede_corso + '</th>';
                hatml1 += '<th></th>';
                var esamebo = esame.esame_bo;
                hatml1 += '<th>' + esamebo.name_bo + '</th>';
                hatml1 += '<th>' + esamebo.codice_bo + '</th>';
                hatml1 += '<th>' + esamebo.anno_bo + '/' +( esamebo.anno_bo + 1) + '</th>';
                hatml1 += '<th>' + esamebo.cfu_bo + '</th>';
                hatml1 += '<th>' + esame.integrazioni_corso + '</th>';
                hatml1+='</tr>'
            }

            $('#tbody').html(hatml1);
        },

        error: function (error) {
            console.log(error);
            alert("Wrong");
        }
    });
}


$(document).ready(function () {
    get_cart()
});

/*
         {
             'id_exam': tem[0].id_exam,
             'sede_corso': tem[0].sede_corso,
             'nome_corso': tem[0].nome_corso,
             'codice_corso': tem[0].codice_corso,
             'anno_corso': tem[0].anno_corso,
             'argomenti_corso': tem[0].argomenti_corso,
             'integrazioni_corso': str(tem[0].integrazioni_corso),
             'cfu_corso': tem[0].cfu_corso,
             'esame_bo': {
                 'id_bo': exambo.id_bo,
                 'codice_bo': exambo.codice_bo,
                 'name_bo': exambo.name_bo,
                 'anno_bo': exambo.anno_bo,
                 'argomenti_bo': exambo.argomenti_bo,
                 'cfu_bo': exambo.cfu_bo,
                 'ssd_bo': exambo.ssd_bo,
                 'sede_bo': exambo.sede_bo,
             },


        <th>
         <th scope="col">Esame</th>
             <th scope="col">Codice</th>
             <th scope="col">Anno accadeico</th>
             <th scope="col">CFU</th>
             <th scope="col">Sede</th>
             <th scope="col"></th>
             <th scope="col">Riconosciuto con</th>
             <th scope="col">Codice</th>
             <th scope="col">Anno accadeico</th>
             <th scope="col">CFU</th>
             <th scope="col">Integrazioni richieste</th>
             <th scope="col">
             </th>

         */