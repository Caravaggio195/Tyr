var host = 'domenico.caravaggio3';

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function sameOrigin(url) {
    var host = document.location.host;
    var protocol = document.location.protocol;
    var sr_origin = "//" + host;
    var origin = protocol + sr_origin;
    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
        (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||

        !(/^(\/\/|http:|https:).*/.test(url));
}

$.ajaxSetup({
    beforeSend: function (xhr, settings) {
        if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
            xhr.setRequestHeader("X-CSRFToken", Cookies.get("csrftoken"));
        }
    }
});

function loginUser() {
    $.ajax({
        url: "/login/",
        type: "POST",
        data: {
            username: $("#id_login-username").val(),
            password: $("#id_login-password").val(),
        },
        success: function (ret) {
            console.log(ret);
            setTimeout(function () {
                location.href = " http://" + host + ".tw.cs.unibo.it/home";
            }, 1000);
        },
        error: function (error) {
            console.log(error);
            alert(error.errors.password);
            $("#login_form").trigger("reset");
        }
    });
}

$(document).ready(function () {
    $("#login_form").on("submit", function (evt) {
        evt.preventDefault();
        loginUser();
    });
});
